'use strict';

const httpRequest = require('request');
const url = require('url');
const mysql = require('mysql');
const _ = require('lodash');
const nconf = require('@dmm/nconf-dmm');

const config = nconf();
const geoApiConfig = config.get('geo-api');
const imtConfig = config.get('imt');
const pool = mysql.createPool(imtConfig['mysql-rw']);

let results = {
  listingsWithValidCityCount: 0,
  listingsWithInvalidCityCount: 0,
  badListingsNoZipCount: 0,
  badListingsNoBetterMatchCount: 0,
  badListingsFixedCount: 0,
  errorCount: 0
};

const removeSpecialCharacters = function(value) {
  if (value) {
    var key = value.toLowerCase();
    key = key.replace(/ |-|\.|\'/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    return key;
  }
};

const delay = (amount = number) => {
  return new Promise((resolve) => {
    setTimeout(resolve, amount);
  });
}

async function loop(listings, conn, callback) {
  for (let i=0; i<listings.length; i++ ) {
    const listing = listings[i];
    console.log('Processing listing ' + i + ' of ' + listings.length);
    if (listing.city != '' && listing.city != null && listing.country_id != '' && listing.country_id != null) {
      let geoApiQuery = {
        locality: listing.city,
        iso: listing.country_id
      }
      if (listing.country_subdivision_id != '' && listing.country_subdivision_id != null) {
        geoApiQuery.iso2 = listing.country_subdivision_id;
      }
      const geoApiOptions = {
        url: url.format({
          protocol: 'http',
          hostname: geoApiConfig.host,
          port: geoApiConfig.port,
          pathname: '/places',
          query: geoApiQuery
        })
      };
      httpRequest(geoApiOptions, function (err, response, body) {
        if (err) {
          console.log('Unable to connect to geo api to validate the current city of the boat with ID=' + listing.product_id + ': ' + JSON.stringify(geoApiOptions));
          console.log(err);
          results.errorCount++;
        } else {
          console.log('Successfully connected to geo api to validate the current city of the boat with ID=' + listing.product_id + ': ' + JSON.stringify(geoApiOptions));
          const apiResult = JSON.parse(body);
          console.log(apiResult.count + ' results obtained from geo api to validate the current city of the boat with ID=' + listing.product_id);

          if (apiResult.count === 0 && listing.postal_code != '' && listing.postal_code != null) {
            results.listingsWithInvalidCityCount++;
            const geoApiOptions2 = {
              url: url.format({
                protocol: 'http',
                hostname: geoApiConfig.host,
                port: geoApiConfig.port,
                pathname: '/places',
                query: {
                  postcode: listing.postal_code,
                  iso: listing.country_id
                }
              })
            };

            httpRequest(geoApiOptions2, function (err2, response2, body2) {
              if (err2) {
                console.log('Unable to connect to geo api to find a better city match for the boat with ID=' + listing.product_id  + ': ' + JSON.stringify(geoApiOptions2));
                console.log(err);
                results.errorCount++;
              } else {
                console.log('Successfully connected to geo api to find a better city match for the boat with ID=' + listing.product_id  + ': ' + JSON.stringify(geoApiOptions2));
                const apiResult2 = JSON.parse(body2);
                console.log(apiResult2.count + ' results obtained from geo api to find a better city match for the boat with ID=' + listing.product_id);
    
                if (apiResult2.count > 0) {
                  //Logic to choose the best result from GEO-API, based in PRIME-71
                  let newCity;
                  if (apiResult2.records[0].locality) {
                    newCity = apiResult2.records[0].locality;
                  }
                  for (var j = 1; j < apiResult2.records.length; j++) {
                    if (_.get(apiResult2.records[j], 'locality', '') !== '' && removeSpecialCharacters(_.get(apiResult2.records[j], 'locality', '')) === removeSpecialCharacters(_.get(listing, 'location.city', ''))) {
                      newCity = apiResult2.records[j].locality;
                      break;
                    }
                  }
                  if (newCity) {
                    newCity = newCity.replace(/'/g, "''");
                    console.log('Updating the city in IMT from ' + listing.city + ' to ' + newCity + ' for the boat with ID=' + listing.product_id);
                    conn.query("update locations set city='"+ newCity  +"' where id=" + listing.location_id, function(queryErr2, rows2) {
                      if (queryErr2) {
                        console.log('Error when trying to update the city in IMT from ' + listing.city + ' to ' + newCity + ' for the boat with ID=' + listing.product_id);
                        results.errorCount++;
                      }
                      else {
                        console.log('Updated the city in IMT from ' + listing.city + ' to ' + newCity + ' for the boat with ID=' + listing.product_id);
                        conn.query("update products set last_updated=now() where id=" + listing.product_id, function(queryErr3, rows3) {
                          if (queryErr3) {
                            console.log('Error when trying to update last_updated field of the boat with ID=' + listing.product_id);
                            results.errorCount++;
                          }
                          else {
                            console.log('Updated last_updated field of the boat with ID=' + listing.product_id);
                            results.badListingsFixedCount++;
                          }
                        });
                      }
                    });
                  }
                  else {
                    console.log('No better city found for the boat with ID=' + listing.product_id);
                    results.badListingsNoBetterMatchCount++;
                  }
                }
                else {
                  console.log('No better city found for the boat with ID=' + listing.product_id);
                  results.badListingsNoBetterMatchCount++;
                }
              }
            });
          }
          else {
            if(apiResult.count > 0) {
              console.log('Valid City found for boat with ID=' + listing.product_id);
              results.listingsWithValidCityCount++;
            }
            else {
              console.log('Invalid city found but no zip code available for boat with ID=' + listing.product_id);
              results.listingsWithInvalidCityCount++;
              results.badListingsNoZipCount++;
            }
          }
        }
      });
    }
    await delay(200);
  }

  callback();
};

pool.getConnection(function(err, conn){
  console.log('Getting the listings from IMT DB');
  conn.query("select p.id as product_id, l.id as location_id, l.city, l.country_id, l.country_subdivision_id, l.postal_code from products p, locations l where l.id=p.location_id and p.status_id='PS_ACTIVE' order by p.id", function(queryErr, rows) {
    const listings = rows;
    console.log('Iterating over ' + listings.length + ' listings');
    loop(listings, conn, function() {
      console.log('Process finished. Results:\n\n' + 
        'Total listings Processed | count ' + listings.length + '\n' +
        'Listings with valid city | count: ' + results.listingsWithValidCityCount + '\n' +
        'Listings with invalid city | count: ' + results.listingsWithInvalidCityCount + '\n' +
        'Listings with invalid city but no zip | count: ' + results.badListingsNoZipCount + '\n' +
        'Listings with invalid city but unable to find better match | count: ' + results.badListingsNoBetterMatchCount + '\n' +
        'Listings with invalid city that were fixed | count: ' + results.badListingsFixedCount + '\n' +
        'Listings that generated an error during processing | count: ' + results.errorCount
      );
      conn.release();
    });
    
  });

});